<!DOCTYPE html>
<html <?php language_attributes(); ?> xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="X-UA-Compatible" content="IE=7">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="web_author" content="Área Local">
    <meta name="theme-color" content="#000000">
    <link rel="icon" href="<?php image_url('favicon.png'); ?>">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <link
        rel="stylesheet"
        type="text/css"
        media="all"
        href="<?php echo get_template_directory_uri(); ?>/style.css"
    >
    <title>
        <?php echo is_front_page() || is_home() ? get_bloginfo('name') : wp_title(''); ?>
    </title>
    <link rel="stylesheet" type="text/css" href="<?= get_template_directory_uri() ?>/assets/css/libs/lightslider.css">
    <link rel="stylesheet" type="text/css" href="<?= get_template_directory_uri() ?>/assets/css/libs/lightgallery.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="<?= get_template_directory_uri() ?>/assets/js/libs/lightslider.js"></script>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <!-- Header -->
    <header role="heading" class="main-header">
        <div class="al-container">
            <a href="<?= get_home_url() ?>"><img src="<?= get_field("logo", "option")['url']?>" alt="Logomarca"></a>
            <div class="traducoes">
                <!-- GTranslate: https://gtranslate.io/ -->
                <style>
                .switcher {font-family:Arial;font-size:12pt;text-align:left;cursor:pointer;width:auto;line-height:17px;}
                .switcher a {text-decoration:none;display:block;font-size:12pt;-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;}
                .switcher a img {vertical-align:middle;display:inline;border:0;padding:0;margin:0;opacity:1;}
                .switcher .selected {position:relative;z-index:9999;}
                .switcher .selected a {color:#666;padding:3px 5px;width:auto;}
                .switcher .selected a:after {top: 22px;height:32px;display:inline-block;position:absolute;right:13px;width:15px;background-position:50%;background-size:11px;background-image:url(<?= get_image_url("down-arrow-icon.webp")?>);background-repeat:no-repeat;content:""!important;transition:all .2s;}
                .switcher .selected a.open:after {-webkit-transform: rotate(-180deg);transform:rotate(-180deg);}
                .switcher .selected a:hover {background:#fff}
                .switcher .option {position:relative;z-index:9998;background-color:#eee;display:none;width:auto;max-height:198px;-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;overflow-y:auto;overflow-x:hidden;}
                .switcher .option a {background-color: #fff;color:#000;padding:3px 5px;}
                .switcher .option a:hover {background:#fff;}
                .switcher .option a.selected {background:#EFEFEF;}
                #selected_lang_name {float: none;}
                .l_name {float: none !important;margin: 0;}
                .switcher .option::-webkit-scrollbar-track{-webkit-box-shadow:inset 0 0 3px rgba(0,0,0,0.3);border-radius:5px;background-color:#f5f5f5;}
                .switcher .option::-webkit-scrollbar {width:5px;}
                .switcher .option::-webkit-scrollbar-thumb {border-radius:5px;-webkit-box-shadow: inset 0 0 3px rgba(0,0,0,.3);background-color:#888;}
                </style>
                <div class="switcher notranslate">
                <div class="selected">
                <a href="#" onclick="jQuery('.option .selected').removeClass('selected');jQuery('.option .nturl img').each(function(){if(jQuery(this).attr('alt')==jQuery('.selected img').attr('alt')){jQuery(this).parent('a').addClass('selected')}});return false;"><img src="//192.168.0.39/one-page-maquira/wp-content/plugins/gtranslate/flags/32/pt-br.png" height="32" width="32" alt="pt" /></a>
                </div>
                <div class="option">
                <a href="#" onclick="jQuery('.option .selected').removeClass('selected');jQuery(this).addClass('selected');doGTranslate('pt|en');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;" title="English" class="nturl"><img data-gt-lazy-src="//192.168.0.39/one-page-maquira/wp-content/plugins/gtranslate/flags/32/en-us.png" height="32" width="32" alt="en" /> </a><a href="#" onclick="jQuery('.option .selected').removeClass('selected');jQuery(this).addClass('selected');doGTranslate('pt|pt');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;" title="Portuguese" class="nturl selected"><img data-gt-lazy-src="//192.168.0.39/one-page-maquira/wp-content/plugins/gtranslate/flags/32/pt-br.png" height="32" width="32" alt="pt" /> </a><a href="#" onclick="jQuery('.option .selected').removeClass('selected');jQuery(this).addClass('selected');doGTranslate('pt|es');jQuery('div.switcher div.selected a').html(jQuery(this).html());return false;" title="Spanish" class="nturl"><img data-gt-lazy-src="//192.168.0.39/one-page-maquira/wp-content/plugins/gtranslate/flags/32/es.png" height="32" width="32" alt="es" /> </a></div>
                </div>
                <script>
                jQuery('.switcher .selected').click(function() {jQuery('.switcher .option a img').each(function() {if(!jQuery(this)[0].hasAttribute('src'))jQuery(this).attr('src', jQuery(this).attr('data-gt-lazy-src'))});if(!(jQuery('.switcher .option').is(':visible'))) {jQuery('.switcher .option').stop(true,true).delay(100).slideDown(500);jQuery('.switcher .selected a').toggleClass('open')}});
                jQuery('.switcher .option').bind('mousewheel', function(e) {var options = jQuery('.switcher .option');if(options.is(':visible'))options.scrollTop(options.scrollTop() - e.originalEvent.wheelDelta/10);return false;});
                jQuery('body').not('.switcher').click(function(e) {if(jQuery('.switcher .option').is(':visible') && e.target != jQuery('.switcher .option').get(0)) {jQuery('.switcher .option').stop(true,true).delay(100).slideUp(500);jQuery('.switcher .selected a').toggleClass('open')}});
                </script>
                <style>#goog-gt-tt{display:none!important;}.goog-te-banner-frame{display:none!important;}.goog-te-menu-value:hover{text-decoration:none!important;}.goog-text-highlight{background-color:transparent!important;box-shadow:none!important;}body{top:0!important;}#google_translate_element2{display:none!important;}</style>
                <div id="google_translate_element2"></div>
                <script>function googleTranslateElementInit2() {new google.translate.TranslateElement({pageLanguage: 'pt',autoDisplay: false}, 'google_translate_element2');}if(!window.gt_translate_script){window.gt_translate_script=document.createElement('script');gt_translate_script.src='https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit2';document.body.appendChild(gt_translate_script);}</script>

                <script>
                function GTranslateGetCurrentLang() {var keyValue = document['cookie'].match('(^|;) ?googtrans=([^;]*)(;|$)');return keyValue ? keyValue[2].split('/')[2] : null;}
                function GTranslateFireEvent(element,event){try{if(document.createEventObject){var evt=document.createEventObject();element.fireEvent('on'+event,evt)}else{var evt=document.createEvent('HTMLEvents');evt.initEvent(event,true,true);element.dispatchEvent(evt)}}catch(e){}}
                function doGTranslate(lang_pair){if(lang_pair.value)lang_pair=lang_pair.value;if(lang_pair=='')return;var lang=lang_pair.split('|')[1];if(GTranslateGetCurrentLang() == null && lang == lang_pair.split('|')[0])return;if(typeof ga=='function'){ga('send', 'event', 'GTranslate', lang, location.hostname+location.pathname+location.search);}var teCombo;var sel=document.getElementsByTagName('select');for(var i=0;i<sel.length;i++)if(sel[i].className.indexOf('goog-te-combo')!=-1){teCombo=sel[i];break;}if(document.getElementById('google_translate_element2')==null||document.getElementById('google_translate_element2').innerHTML.length==0||teCombo.length==0||teCombo.innerHTML.length==0){setTimeout(function(){doGTranslate(lang_pair)},500)}else{teCombo.value=lang;GTranslateFireEvent(teCombo,'change');GTranslateFireEvent(teCombo,'change')}}
                if(GTranslateGetCurrentLang() != null)jQuery(document).ready(function() {var lang_html = jQuery('div.switcher div.option').find('img[alt="'+GTranslateGetCurrentLang()+'"]').parent().html();if(typeof lang_html != 'undefined')jQuery('div.switcher div.selected a').html(lang_html.replace('data-gt-lazy-', ''));});
                </script>

                
            </div>
        </div>
    </header>
    <!-- Wrapper -->
    <div id="wrapper">
