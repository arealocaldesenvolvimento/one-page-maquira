<?php get_header() ?>
    <div class="all-produtos">
        <div class="al-container">
            <h1 class="title">Produtos</h1>
            <div class="row-produtos">
                <div class="content">
                    <div class="produtos">
                        <div class="al-container">
                            <?php
                            $produtos = new WP_Query(array(
                                'post_type' => 'produtos',
                                'posts_per_page'	=> -1,
                                'meta_key' => 'destaque'
                            ));
                            $cont=0;
                            while($produtos->have_posts()):
                                $cont++;
                                $produtos->the_post();
                                ?>   
                                <div id="<?= $cont?>" class="produto" style="background-image: url('<?= get_image_url("bottom-border.png")?>');">        
                                    <a href="<?= get_page_link() ?>">
                                        <section>
                                            <div class="img-produto">
                                                <img src="<?= get_field('galeria_thumbnail')[0]['url']?>" alt="">
                                            </div>
                                            <div class="texto-produto">
                                                <h3><?= get_the_title() ?></h3>
                                            </div>
                                        </section>
                                    </a>
                                </div>
                            <?php
                                endwhile;
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer() ?>