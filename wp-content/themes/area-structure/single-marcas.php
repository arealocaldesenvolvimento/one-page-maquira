<?php get_header() ?>
    <div class="al-container">
        <div class="single-container">
            <img src="<?= get_field("marcas")['logo']['url'] ?>" alt="Logomarca" class="logo">
            <div class="texto" id="texto-<?= $cont; ?>">
                <?= get_field("marcas")['texto']?>
            </div>
            <ul class="links">
                <?php if(get_field("marcas")['facebook_link']!=""): ?>
                    <a target="_blank" href="<?= get_field("marcas")['facebook_link']; ?>"><li><img src="<?= get_image_url("facebook-icon.png")?>" alt="Icone Facebook"></li></a>
                <?php endif; ?>
                <?php if(get_field("marcas")['instagram_link']!=""): ?>
                    <a target="_blank" href="<?= get_field("marcas")['instagram_link']; ?>"><li><img src="<?= get_image_url("instagram-icon.png")?>" alt="Icone Instagram"></li></a>
                <?php endif; ?>
                <?php if(get_field("marcas")['linkedin_link']!=""): ?>
                    <a target="_blank" href="<?= get_field("marcas")['linkedin_link']; ?>"><li><img src="<?= get_image_url("linkedin-icon.png")?>" alt="Icone Linkedin"></li></a>
                <?php endif; ?>
                <?php if(get_field("marcas")['site_url']!=""): ?>
                    <a target="_blank" href="<?= get_field("marcas")['site_url']; ?>"><li><img src="<?= get_image_url("acessar-icon.png")?>" alt="Icone Linkedin">Acesse<br> o site</li></a>
                <?php endif; ?>
            </ul>
            <div class="break"></div>
            <div class="marca">
                <h2 class="nome" id="nome-1" onclick="openMarca(1)"><img src="<?= get_image_url("right-arrow-icon.png") ?>" alt="ìcone under"><?= get_the_title(); ?></h2>
                <div class="open" id="open-1">
                    <h3>Nacional</h3>
                    <ul>
                        <?php foreach(get_field("contato_nacional") as $nacional): ?>
                            <li>
                                <a href="https://api.whatsapp.com/send?phone=<?= $nacional['whatsapp'] ?>" target="_blank">
                                    <img src="<?= get_image_url("whatsapp-icon.png") ?>" alt="Ícone Whatsapp">
                                </a>
                                <a href="mailto:<?= $nacional['e-mail'] ?>" target="_blank">
                                    <img src="<?= get_image_url("email-icon.jpg") ?>" alt="Ícone E-mail">
                                </a>
                                <?= $nacional['regiao'] ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                    <h3>Internacional</h3>
                    <ul>
                        <?php foreach(get_field("contato_internacional") as $nacional): ?>
                            <li>
                                <a href="https://api.whatsapp.com/send?phone=<?= $nacional['whatsapp'] ?>" target="_blank">
                                    <img src="<?= get_image_url("whatsapp-icon.png") ?>" alt="Ícone Whatsapp">
                                </a>
                                <a href="mailto:<?= $nacional['e-mail'] ?>" target="_blank">
                                    <img src="<?= get_image_url("email-icon.jpg") ?>" alt="Ícone E-mail">
                                </a>
                                <?= $nacional['regiao'] ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>                 
<?php get_footer() ?>