<?php

flush_rewrite_rules();

/**
 * Example post-type.
 */
function depoimentos(): void
{
    register_post_type('Depoimentos', [
        'labels' => [
            'name' => _x('Depoimentos', 'Depoimentos'),
            'singular_name' => _x('Depoimento', 'Depoimento'),
            'add_new' => __('Adicionar novo depoimento'),
            'add_new_item' => __('Adicionar novo depoimento'),
            'edit_item' => __('Editar depoimento'),
            'new_item' => __('Novo depoimento'),
            'view_item' => __('Ver depoimento'),
            'not_found' => __('Nenhum depoimento encontrado'),
            'not_found_in_trash' => __('Nenhum depoimento encontrado na lixeira'),
            'parent_item_colon' => '',
            'menu_name' => 'Depoimentos',
        ],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 4,
        'menu_icon' => 'dashicons-book',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'supports' => ['title', 'thumbnail', 'editor', 'custom-fields', 'revisions'],
    ]);
}
add_action('init', 'depoimentos');
function marcas(): void
{
    register_post_type('Marcas', [
        'labels' => [
            'name' => _x('Marcas', 'Marcas'),
            'singular_name' => _x('Marca', 'Marca'),
            'add_new' => __('Adicionar nova Marca'),
            'add_new_item' => __('Adicionar nova marca'),
            'edit_item' => __('Editar marca'),
            'new_item' => __('Nova marca'),
            'view_item' => __('Ver marca'),
            'not_found' => __('Nenhum marca encontrado'),
            'not_found_in_trash' => __('Nenhuma marca encontrado na lixeira'),
            'parent_item_colon' => '',
            'menu_name' => 'Marcas',
        ],
        'hierarchical' => false,
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 4,
        'menu_icon' => 'dashicons-book',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'can_export' => true,
        'has_archive' => true,
        'exclude_from_search' => false,
        'publicly_queryable' => true,
        'capability_type' => 'post',
        'supports' => ['title', 'thumbnail', 'editor', 'custom-fields', 'revisions'],
    ]);
}
add_action('init', 'marcas');
