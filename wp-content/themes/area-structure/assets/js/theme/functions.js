/**
 * Global theme functions
 */

/**
 * @description Exemplo ajax
 */
const exampleAjax = async () => {
	const stateResponse = await fetch(`${window.apiUrl}/estados`)
	const { states, message } = await stateResponse.json()
	console.log(states, message)
}

window.onload = async () => {
	/**
	 * Contact form 7 alerts
	 */
	const form = document.querySelector('.wpcf7')
	if (form) {
		form.addEventListener('wpcf7mailsent', () => {
			Swal.fire({
				icon: 'success',
				title: 'Sucesso!',
				text: 'Mensagem enviada!',
			})
		})

		form.addEventListener('wpcf7mailfailed', () => {
			Swal.fire({
				icon: 'error',
				title: 'Ocorreu um erro!',
				text: 'Se o erro persistir, favor contatar o suporte.',
			})
		})
	}
}
$(document).ready(()=>{
	var marcasSlider = $('#marcas-slider').lightSlider({
		item: 4,
        autoWidth: false,
        slideMove: 1, 
        slideMargin: 0,
        speed: 2000, 
        auto: false,
        loop: true,
        slideEndAnimation: true,
        pause: 2000,
        keyPress: false,
		adaptiveHeight:true,
		controls: true,
        prevHtml: '<img src="'+window.alUrl.homeUrl+'/wp-content/themes/area-structure/assets/images/prev.webp">',
        nextHtml: '<img src="'+window.alUrl.homeUrl+'/wp-content/themes/area-structure/assets/images/next.webp">',
        pager: false,
		enableDrag: false,
    });
	var marcasMobileSlider = $('#marcas-mobile-slider').lightSlider({
		item: 1,
        autoWidth: false,
        slideMove: 1, 
        slideMargin: 0,
        speed: 2000, 
        auto: true,
        loop: true,
        slideEndAnimation: true,
        pause: 2000,
        keyPress: false,
		adaptiveHeight:true,
		controls: true,
        prevHtml: '<img src="'+window.alUrl.homeUrl+'/wp-content/themes/area-structure/assets/images/prev.webp">',
        nextHtml: '<img src="'+window.alUrl.homeUrl+'/wp-content/themes/area-structure/assets/images/next.webp">',
        pager: false,
		enableDrag: false,
    });
	var depoimentosSlider = $('#depoimentos-slider').lightSlider({
		item: 3,
        autoWidth: false,
        slideMove: 1, 
        slideMargin: 0,
        speed: 2000, 
        auto: true,
        loop: true,
        slideEndAnimation: true,
        pause: 2000,
        keyPress: false,
		adaptiveHeight:true,
		controls: true,
        prevHtml: '<img src="'+window.alUrl.homeUrl+'/wp-content/themes/area-structure/assets/images/prev.webp">',
        nextHtml: '<img src="'+window.alUrl.homeUrl+'/wp-content/themes/area-structure/assets/images/next.webp">',
        pager: false,
		enableDrag: false,
    });
	var depoimentosMobileSlider = $('#depoimentos-mobile-slider').lightSlider({
		item: 1,
        autoWidth: false,
        slideMove: 1, 
        slideMargin: 0,
        speed: 2000, 
        auto: true,
        loop: true,
        slideEndAnimation: true,
        pause: 2000,
        keyPress: false,
		adaptiveHeight:true,
		controls: true,
        prevHtml: '<img src="'+window.alUrl.homeUrl+'/wp-content/themes/area-structure/assets/images/prev.webp">',
        nextHtml: '<img src="'+window.alUrl.homeUrl+'/wp-content/themes/area-structure/assets/images/next.webp">',
        pager: false,
		enableDrag: false,
    });
});