    </div>

    <!-- Footer -->
    <footer class="main-footer">
        <div class="al-container">
            <div class="collum-footer">
            <a href="<?= get_home_url(); ?>"><img src="<?= get_field("logo", "option")['url']?>" alt="Logomarca"></a>
                <a targer="_blank" href="<?= get_field("localização", "option")['link']?>"><?= get_field("localização", "option")['texto']?></a>
            </div>
            <div class="collum-footer">
                <ul class="lista-links">
                    <li>
                        <img src="<?= get_image_url("right-arrow-icon.webp") ?>" alt="Ícone seta">
                        <a target="_blank" href="<?= get_field("trabalhe_conosco", "option")?>">Trabalhe conosco</a>
                    </li>
                    <li>
                        <img src="<?= get_image_url("right-arrow-icon.webp") ?>" alt="Ícone seta">
                        <a download target="_blank" href="<?= get_field("codigo_de_de_etica_e_conduta", "option")['url']?>">Código de ética e conduta</a>
                    </li>
                    <li>
                        <img src="<?= get_image_url("right-arrow-icon.webp") ?>" alt="Ícone seta">
                        <a target="_blank" href="<?= get_field("politica_de_privacidade", "option")?>">Politica de Privacidade</a>
                    </li>
                </ul>
            </div>
            <div class="collum-footer marca-collum">
                <h3 class="footer-title">Contatos</h3>
                <div class="marcas">
                    <?php
                        $marcas = new WP_Query(array(
                            'post_type' => 'marcas',
                            'posts_per_page'	=> -1
                            ));
                        $cont=0;
                        while($marcas->have_posts()):
                            $cont++;
                            $marcas->the_post();
                            ?>   
                                <div class="marca">
                                    <h2 class="nome" id="nome-<?= $cont; ?>" onclick="openMarca(<?= $cont; ?>)"><?= get_the_title(); ?></h2>
                                    <div class="open" id="open-<?= $cont?>">
                                        <h3>Nacional</h3>
                                        <ul>
                                            <?php foreach(get_field("contato_nacional") as $nacional): ?>
                                                <li>
                                                    <a href="https://api.whatsapp.com/send?phone=<?= $nacional['whatsapp'] ?>" target="_blank">
                                                        <img src="<?= get_image_url("whatsapp-icon.webp") ?>" alt="Ícone Whatsapp">
                                                    </a>
                                                    <a href="mailto:<?= $nacional['e-mail'] ?>" target="_blank">
                                                        <img src="<?= get_image_url("email-icon.webp") ?>" alt="Ícone E-mail">
                                                    </a>
                                                    <?= $nacional['regiao'] ?>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                        <h3>Internacional</h3>
                                        <ul>
                                            <?php foreach(get_field("contato_internacional") as $nacional): ?>
                                                <li>
                                                    <a href="https://api.whatsapp.com/send?phone=<?= $nacional['whatsapp'] ?>" target="_blank">
                                                        <img src="<?= get_image_url("whatsapp-icon.webp") ?>" alt="Ícone Whatsapp">
                                                    </a>
                                                    <a href="mailto:<?= $nacional['e-mail'] ?>" target="_blank">
                                                        <img src="<?= get_image_url("email-icon.webp") ?>" alt="Ícone E-mail">
                                                    </a>
                                                    <?= $nacional['regiao'] ?>
                                                </li>
                                            <?php endforeach; ?>
                                        </ul>
                                    </div>
                                </div>
                            <?php
                        endwhile;
                        $home = new WP_Query(array('page' => 'home'));
                        $home->the_post();
                    ?>
                </div>
            </div>
        </div>
    </footer>

    <!-- Scripts -->
    <script>
        /**
         * @description global JS variables
         */
        window.alUrl = {
            templateUrl: '<?php echo addslashes(get_bloginfo('template_url')); ?>',
            homeUrl: '<?php echo addslashes(home_url()); ?>'
        }
	    window.apiUrl = `${window.alUrl.homeUrl}/wp-json/api`
    </script>
    <script src="https://kit.fontawesome.com/4800576786.js" crossorigin="anonymous"></script>

    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/public/js/vendor.js"></script>
    <script async type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/public/js/app.js"></script>
    <?php wp_footer(); ?>
</body>
</html>
