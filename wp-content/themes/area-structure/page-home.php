<?php get_header() ?>
    <section class="detalhe d-left">
        <img src="<?= get_image_url("bottom-detail.webp") ?>" alt="Detalhe">
    </section>
    <div class="banner">
        <?= do_shortcode('[rev_slider alias="slider-1"][/rev_slider]'); ?>
    </div>
    <div class="banner-mobile">
        <?= do_shortcode('[rev_slider alias="slider-2"][/rev_slider]'); ?>
    </div>
    <section class="detalhe">
        <img src="<?= get_image_url("bottom-detail.webp") ?>" alt="Detalhe">
    </section>
    <section class="descricao">
        <div class="al-container">
            <div class="texto">
                <?= get_field("descricao") ?>
            </div>
            <div class="cards">
                <?php foreach(get_field("cards") as $card): ?>
                    <div class="card">
                        <div class="front">
                            <h2 class="title"><?= $card['title'] ?></h2>
                        </div>
                        <div class="back">
                            <div class="texto"><?= $card['texto'] ?></div>
                        </div>
                    </div>
                <?php endforeach;?>
            </div>
        </div>
    </section>
    <section class="midle-banner">
        <div class="al-container">
            <div class="texto">
                <?= get_field("banner_central") ?>
            </div>
        </div>
    </section>
    <section class="marcas">
        <div class="al-container">
            <ul id="marcas-slider">
                <?php
                    $marcas = new WP_Query(array(
                        'post_type' => 'marcas',
                        'posts_per_page'	=> -1
                        ));
                    $cont=0;
                    while($marcas->have_posts()):
                        $cont++;
                        $marcas->the_post();
                        ?>   
                            <li>
                                <img src="<?= get_field("marcas")['logo']['url'] ?>" alt="Logomarca" class="logo">
                                <div class="texto" id="texto-<?= $cont; ?>">
                                    <?= get_field("marcas")['texto']?>
                                </div>
                                <ul class="links">
                                    <?php if(get_field("marcas")['facebook_link']!=""): ?>
                                        <a target="_blank" href="<?= get_field("marcas")['facebook_link']; ?>"><li><img src="<?= get_image_url("facebook-icon.webp")?>" alt="Icone Facebook"></li></a>
                                    <?php endif; ?>
                                    <?php if(get_field("marcas")['instagram_link']!=""): ?>
                                        <a target="_blank" href="<?= get_field("marcas")['instagram_link']; ?>"><li><img src="<?= get_image_url("instagram-icon.webp")?>" alt="Icone Instagram"></li></a>
                                    <?php endif; ?>
                                    <?php if(get_field("marcas")['linkedin_link']!=""): ?>
                                        <a target="_blank" href="<?= get_field("marcas")['linkedin_link']; ?>"><li><img src="<?= get_image_url("linkedin-icon.webp")?>" alt="Icone Linkedin"></li></a>
                                    <?php endif; ?>
                                    <?php if(get_field("marcas")['site_url']!=""): ?>
                                        <a target="_blank" href="<?= get_field("marcas")['site_url']; ?>"><li><img src="<?= get_image_url("acessar-icon.webp")?>" alt="Icone Linkedin">Acesse<br> o site</li></a>
                                    <?php endif; ?>
                                </ul>
                            </li>
                        <?php
                    endwhile;
                    $home = new WP_Query(array('page' => 'home'));
                    $home->the_post();
                ?>
            </ul>
            <ul id="marcas-mobile-slider">
                <?php
                    $marcas = new WP_Query(array(
                        'post_type' => 'marcas',
                        'posts_per_page'	=> -1
                        ));
                    $cont=0;
                    while($marcas->have_posts()):
                        $cont++;
                        $marcas->the_post();
                        ?>   
                            <li>
                                <img src="<?= get_field("marcas")['logo']['url'] ?>" alt="Logomarca" class="logo">
                                <div class="texto" id="texto-mobile-<?= $cont; ?>">
                                    <?= get_field("marcas")['texto']?>
                                </div>
                                <a id="ler-mais-mobile-<?= $cont; ?>" href="<?= get_permalink() ?>" class="leia-mais">Leia Mais</a>
                                <ul class="links">
                                    <?php if(get_field("marcas")['facebook_link']!=""): ?>
                                        <a target="_blank" href="<?= get_field("marcas")['facebook_link']; ?>"><li><img src="<?= get_image_url("facebook-icon.webp")?>" alt="Icone Facebook"></li></a>
                                    <?php endif; ?>
                                    <?php if(get_field("marcas")['instagram_link']!=""): ?>
                                        <a target="_blank" href="<?= get_field("marcas")['instagram_link']; ?>"><li><img src="<?= get_image_url("instagram-icon.webp")?>" alt="Icone Instagram"></li></a>
                                    <?php endif; ?>
                                    <?php if(get_field("marcas")['linkedin_link']!=""): ?>
                                        <a target="_blank" href="<?= get_field("marcas")['linkedin_link']; ?>"><li><img src="<?= get_image_url("linkedin-icon.webp")?>" alt="Icone Linkedin"></li></a>
                                    <?php endif; ?>
                                    <?php if(get_field("marcas")['site_url']!=""): ?>
                                        <a target="_blank" href="<?= get_field("marcas")['site_url']; ?>"><li><img src="<?= get_image_url("acessar-icon.webp")?>" alt="Icone Linkedin">Acesse<br> o site</li></a>
                                    <?php endif; ?>
                                </ul>
                            </li>
                        <?php
                    endwhile;
                    $home = new WP_Query(array('page' => 'home'));
                    $home->the_post();
                ?>
            </ul>
        </div>
    </section>
    <section class="under-banner">
        <div class="al-container">
            <div class="right">
                <h2 class="title">Política da qualidade</h2>
                <div class="texto">
                    <?= get_field("politica_de_qualidade") ?>
                </div>
            </div>
        </div>
    </section>
    <section class="depoimentos">
        <h2 class="title">Depoimentos</h2>
        <div class="al-container">
            <ul id="depoimentos-slider">
                <?php
                    $depoimentos = new WP_Query(array(
                        'post_type' => 'depoimentos',
                        'posts_per_page'	=> -1
                    ));
                    $cont=0;
                    while($depoimentos->have_posts()):
                        $cont++;
                        $depoimentos->the_post();
                        ?>   
                            <li>
                                <div class="texto">
                                    <?= get_field("depoimento")?>
                                </div>
                                <div class="perfil">
                                    <img src="<?= get_field("foto")['url']?>" alt="Foto de perfil">
                                    <div class="texto">
                                        <h3 class="nome"><?= get_field("nome")?></h3>
                                        <h3 class="cargo"><?= get_field("instituicao-ou-cargo") ?></h3>
                                    </div>
                                </div>
                            </li>
                        <?php
                    endwhile;
                    $home = new WP_Query(array('page' => 'home'));
                    $home->the_post();
                ?>
            </ul>
            <ul id="depoimentos-mobile-slider">
                <?php
                    $depoimentos = new WP_Query(array(
                        'post_type' => 'depoimentos',
                        'posts_per_page'	=> -1
                    ));
                    $cont=0;
                    while($depoimentos->have_posts()):
                        $cont++;
                        $depoimentos->the_post();
                        ?>   
                            <li>
                                <div class="texto">
                                    <?= get_field("depoimento")?>
                                </div>
                                <div class="perfil">
                                    <img src="<?= get_field("foto")['url']?>" alt="Foto de perfil">
                                    <div class="texto">
                                        <h3 class="nome"><?= get_field("nome")?></h3>
                                        <h3 class="cargo"><?= get_field("instituicao-ou-cargo") ?></h3>
                                    </div>
                                </div>
                            </li>
                        <?php
                    endwhile;
                    $home = new WP_Query(array('page' => 'home'));
                    $home->the_post();
                ?>
            </ul>
        </div>
    </section>
    <section class="lideranca-banner">
        <div class="al-container">
            <h2 class="title">Liderança</h2>
        </div>
    </section>
    <section class="lideranca-texto">
        <div class="al-container">
            <h2 class="title"><?= get_field("lideranca")['titulo'] ?></h2>
            <div class="texto"><?= get_field("lideranca")['texto'] ?></div>
        </div>
    </section>
    <section class="org">
        <div class="al-container">
            <h2 class="title">Conselho administrativo</h2>
        </div>
    </section>
    <?= do_shortcode("[wpda_org_chart tree_id=7 theme_id=1]")?>
    <section class="org">
        <div class="al-container">
            <h2 class="title">Diretoria</h2>
        </div>
    </section>
    <?= do_shortcode("[wpda_org_chart tree_id=6 theme_id=1]")?>
<?php get_footer() ?>